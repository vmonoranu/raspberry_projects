/* *********************************************************************
 *
 *      Author: Vlad Monoranu
 *      Source: main.c
 *      Description: Server side of app will implement all controllers for different plants
 *
 *
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../common/build_options.h"
#include "server_misc.h"

#ifndef __arm__
#include <x86intrin.h>
#endif


int
send_to_client_data(struct sockaddr* client, uint32_t client_struct_len, int server_socket)
{
    char buffer[BUFFER_SIZE];

    for (int label = 0; label < NUMBER_OF_LABELS; ++label)
    {
        unsigned long long timestamp = 0;

#ifndef __arm__
        timestamp = __rdtsc();
#else
        timestamp  = 0xDEADBEEF;
#endif
        sprintf(buffer, "Label: %d Timestamp: %llu", label, timestamp);
        printf("About to send: %s\n", buffer);
        sendto(server_socket,buffer, strlen(buffer),0, client, client_struct_len);
    }

    return 0;
}

int
main (int argc, char* argv[])
{
    init_server();
    init_controllers_threads();

    run_server_dispatcher();

    return 0;
}