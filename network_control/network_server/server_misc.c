#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <math.h>

#include <time.h>
#include "server_misc.h"
#include "plants.h"
#include "../common/build_options.h"
#include "../common/messages.h"
#include "../common/synch_wrapper.h"


#define KEEP_PACKET         0
#define SKIP_PACKET         1

#define PLOT_IP     "192.168.1.103"
#define PLOT_PORT   33333


int g_server_socket = 0;  // globally socket descriptor shared between all threads
struct sockaddr_in g_server_address;
struct sockaddr_in g_plot_address;
pthread_t g_controller_threads[NUMBER_OF_CONTROLLERS];


int
init_server ()
{
    int srv_address = 0;
    //
    //  Creating the server socket
    //
    if (-1 == (g_server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
    {
        /// should log these stuff
        exit(1);
    }

    //  Set sockaddr_in fields to ZERO
    memset((char*) &g_server_address, 0, sizeof(g_server_address));

    g_server_address.sin_family = AF_INET;
    g_server_address.sin_port   = htons(SERVER_PORT);
    g_server_address.sin_addr.s_addr = htonl(INADDR_ANY);

    if (-1 == bind(g_server_socket, (struct sockaddr*) &g_server_address, sizeof(g_server_address)))
    {
        //binding failed
        exit(1);
    }

    //
    //  Init plot sending packets
    //

    memset((char *)&g_plot_address, 0, sizeof(g_plot_address));
    g_plot_address.sin_family = AF_INET;
    g_plot_address.sin_port = htons(PLOT_PORT);

    if (0 == (srv_address = inet_addr(PLOT_IP)))
    {
        //failed to convert ASCII to network address
        exit(-1);
    }

    g_plot_address.sin_addr.s_addr = srv_address;


    return 0;
}


int
init_controllers_threads ()
{
    int status = 0;
    int default_lock_wrapper = TYPE_SPINLOCK;

    for (int i = 0; i < NUMBER_OF_CONTROLLERS; ++i)
    {
        init_lock_wrapper(&g_controllers_data[i].data_acquisition_lock, default_lock_wrapper);
        init_lock_wrapper(&g_controllers_data[i].data_processing_lock, default_lock_wrapper);

        //acquire_lock_wrapper(&g_controllers_data[i].data_acquisition_lock);
        //acquire_lock_wrapper(&g_controllers_data[i].data_processing_lock);

        status = pthread_create(g_controller_threads + i, NULL, &generic_controller_thread, NULL);
        if (status)
        {
            /// pthread_create failed!
            exit(-1);
        }
    }


    return 0;
}

int
clean_controllers_threads ()
{
    for (int i = 0; i < NUMBER_OF_CONTROLLERS; ++i)
    {
        ///  sigkill all threads SHOULD TEST THIS!!!
        pthread_cancel(g_controller_threads[i]);

        //  clean buffers allocated by the dispatcher thread
        free(g_controllers_data[i].packet_buffer);

        //  destroy all locks
        clean_lock_wrapper(&g_controllers_data[i].data_processing_lock);
        clean_lock_wrapper(&g_controllers_data[i].data_acquisition_lock);
    }



    return 0;
}


int
run_server_dispatcher ()
{
    struct sockaddr_in client_address;
    char buffer[BUFFER_SIZE];
    
//    int status = 0;
    int controller_thread_index = 0;
    int receive_length = 0;
    int packet_size = 0;
    int timestamp = 0;
    int latest_timestamp = 0;
    uint32_t actual_length = sizeof(client_address);


    printf("Entering dispatcher loop...\n");
    while(1)
    {
        receive_length = recvfrom(g_server_socket, buffer, BUFFER_SIZE, 0, (struct sockaddr*)&client_address, &actual_length);
        if (-1 == receive_length)
        {
            exit(1);
        }
        //  get the packet size
        packet_size = ((int*)buffer)[0];
        timestamp = ((int*)buffer)[1];

        // here we want to match the ip address with the correct index to identify the correct controller thread
        {


        }

	//stop everything
        if (packet_size == 0xDEADBEEF && timestamp == 0xDEADBEEF)
	{
		clean_controllers_threads();
		latest_timestamp = -1;
		timestamp = 0;
	}
        if (latest_timestamp < timestamp)
        {
            latest_timestamp = timestamp;
        }
        else
        {
            continue;
        }

        if (NULL == g_controllers_data[controller_thread_index].packet_buffer)
        {
            g_controllers_data[controller_thread_index].packet_buffer = (char*) malloc(sizeof(BUFFER_SIZE));
        }

        acquire_lock_wrapper(&g_controllers_data[controller_thread_index].data_processing_lock);
        memcpy(g_controllers_data[controller_thread_index].packet_buffer, buffer, packet_size);

            g_controllers_data[controller_thread_index].client_address = client_address;
            g_controllers_data[controller_thread_index].client_struct_len = actual_length;

        release_lock_wrapper(&g_controllers_data[controller_thread_index].data_processing_lock);
    }
    // here should send kill message to all controller threads and wait for their completion
    clean_controllers_threads();

    return 0;
}


int
generic_packet_filtering(char *data_packet)
{
    int status = KEEP_PACKET;
//    int timestamp = ((int*)data_packet)[1];
    
    return status;
}

//
//  This should serve as a framework for future control strategies:
//          ---DOCUMENT THIS---
void*
generic_controller_thread (void* arg)
{
    int status = 0;
    char command_packet[BUFFER_SIZE];
    char local_data_packet[64];
    char plot_packet[64];
    PTHREAD_CONTROLLER_DATA thread_local_data = &g_controllers_data[(size_t)arg];
    double reference = 0.0;
    double time_s = 0.0;

    struct timespec time_req, time_rem;
    struct timespec delay_req, delay_rem;

    //  sleep for specified amount of seconds
    time_req.tv_sec = 0;
    time_req.tv_nsec = thread_local_data->sampling_rate * 1000000000.0; //convert from seconds to ns

    delay_req.tv_sec = 0;
    delay_req.tv_nsec = 0.05 * 1000000000.0; //convert from seconds to ns
    while(1)
    {
        nanosleep(&time_req, &time_rem);
	
	time_s += thread_local_data->sampling_rate;
        reference = thread_local_data->reference_generator(time_s);
	
	if (thread_local_data->packet_buffer == NULL)
	{
		continue;
	}


        acquire_lock_wrapper(&thread_local_data->data_processing_lock);

	int packet_size = ((int*)thread_local_data->packet_buffer)[0];
	memcpy(local_data_packet, thread_local_data->packet_buffer, packet_size);

	release_lock_wrapper(&thread_local_data->data_processing_lock);
        
        //  filter packets !
        status = generic_packet_filtering(local_data_packet);
        
        if (status == SKIP_PACKET)
        {
      //      release_lock_wrapper(&thread_local_data->data_processing_lock);
        //    continue;
        }

        
        // compute the control
        status = thread_local_data->compute_response(local_data_packet, command_packet, reference);

        if (status == 0)
        {
        }
        //compensator function
        thread_local_data->fn_compensator(command_packet);

	//printf("%f\n", ((double*)command_packet)[1]);
        // send command packet now!!!
        /// BEWARE HARDCODED SIZE
        status = sendto(g_server_socket, command_packet, ((int*)command_packet)[0], 0, (struct sockaddr*)&thread_local_data->client_address, thread_local_data->client_struct_len);


        if (thread_local_data->enable_plotting) {
            thread_local_data->fn_send_udp_plot_packet(plot_packet, local_data_packet, command_packet);

            ///!!!  hardcoded size
            // send packet to plot!
            status = sendto(g_server_socket, plot_packet, 40, 0, (struct sockaddr *) &g_plot_address,
                            thread_local_data->client_struct_len);
        }

        
    }

    return NULL;
}


