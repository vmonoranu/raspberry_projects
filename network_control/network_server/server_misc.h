#ifndef SERVER_MISC_H
#define SERVER_MISC_H



int init_server ();
int run_server_dispatcher ();
int init_controllers_threads ();
void* generic_controller_thread (void *arg);


#endif //SERVER_MISC_H
