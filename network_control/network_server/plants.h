#ifndef _PLANTS_H
#define _PLANTS_H

#include "../common/features.h"
#include "../common/synch_wrapper.h"


#include <stdbool.h>

//      here we include our controllers
#include "flexible_link_controller.h"

#define FLEXIBLE_LINK   "127.0.0.1"


typedef struct _THREAD_CONTROLLER_DATA
{
    char *ip_address;
    char *packet_buffer;
    double sampling_rate;          //this guy is in seconds
    bool enable_plotting;
    struct sockaddr_in client_address;
    uint32_t client_struct_len;
    LOCK_WRAPPER data_acquisition_lock;
    LOCK_WRAPPER data_processing_lock;
    FN_CONTROLLER_PROCESS_DATA compute_response;
    FN_CONTROLLER_GENERATE_REFERENCE reference_generator;
    FN_CONTROLLER_COMPENSATOR fn_compensator;
    FN_CONTROLLER_UDP_PLOT fn_send_udp_plot_packet;
}THREAD_CONTROLLER_DATA, *PTHREAD_CONTROLLER_DATA;


static THREAD_CONTROLLER_DATA g_controllers_data[] __attribute__((__unused__)) =
        {
            {FLEXIBLE_LINK, NULL, 0.01, true, {0}, 0, {0}, {0},
                    flexible_link_controller, flexible_link_reference_generator,
                    flexible_link_compensator, flexible_link_udp_plot_packet},

            /*  -------TEMPLATE----------
             *  {<IP_STRING_PLANT>, NULL, <SAMPLING_RATE_SECONDS>, <IF_PLOTTING_NEEDED>, {0}, 0, {0}, {0},
                    <PLANT_CONTROLLER_FUNCTION>, <PLANT_REFERENCE_GENERATOR_FUNCTION>,
                    <PLANT_COMPENSATOR_FUNCTION>, <PLANT_UDP_PLOT_FUNCTION>},
             * */
        };


#define NUMBER_OF_CONTROLLERS sizeof_array(g_controllers_data)




#endif
