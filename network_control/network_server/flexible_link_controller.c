#include <stdio.h>
#include <string.h>
#include <math.h>
#include "flexible_link_controller.h"

/*
    ------------------------------------------------------------------------------------------
    -   Flexible link packet format in header!                                               -
    ------------------------------------------------------------------------------------------
*/

#define KONSTANT	5.0
#define PI		            3.141599265359


//double gains[4] = {20 / KONSTANT, -93.56 / KONSTANT, 3.36 / KONSTANT, 0.96 / KONSTANT};//   R = 1
double gains[4] = {6.32, -25.13, 0.85, 0.08};  // R = 10
//double gains[4] = {6.32, 0.0, 0.85, 0.0};  // R = 10
//double gains[4] = {6.32, 0, 0.55, 0};  // R = 10  Q=[400 100 3 2]
//double gains[4] = {3.46, 0, 0.25, 0};  // R = 10  Q= 0.3 * [400 10000 3 2]
//double gains[4] = {0.3,0.2,0.2,0.2};
double k_gage = 0.0526;
double sampling_period = 0.01;	//	seconds



//  Some global variables for plot
//
double g_plot_reference = 0.0;
double g_plot_filtered_deflection = 0.0;


double previous_theta_angle = 0.0;
double previous_deflection = 0.0;
double previous_filtered_deflection = 0.0;
double previous_filtered_dot_theta_angle = 0.0;
double previous_filtered_dot_deflection = 0.0;
double previous_filtered_dot_reference = 0.0;

double
filter_deflection(double deflection, double previous_filtered_deflection)
{
    double filtered_deflection = 0.0;
 
    filtered_deflection = (previous_filtered_deflection + 30.0 * sampling_period * deflection) / (1.0 + 30.0 * sampling_period);
       
    
    return filtered_deflection;
}


double
filter_me(double current_sample, double previous_diff_sample, double A)
{
    double result = 0.0;
    
    result = sampling_period * A * (current_sample) + previous_diff_sample;
    result /= (sampling_period * A + 1);

    return result;
}


double
diff_and_filter(double current_sample, double previous_sample,
	double previous_diff_sample, double A)
{
    double result = 0.0;

    result = A * (current_sample - previous_sample) + previous_diff_sample;
    result /= (sampling_period * A + 1);

    return result;
}

double sign = 1.0;
double period_s = 10.999999;
double step_s = 10.0;
double reference = 0.0;
double global_time = 0.0;

double
flexible_link_reference_generator(double time_s)
{
//      double frequency = 0.2;
//      double amplitude = PI / 2.0;
//      double reference = amplitude * sin(2.0 * PI * frequency * time_s);
//        temp = PI / 2.0;
// reference_angle = temp;


    double amplitude = PI / 4.0;
    
    global_time = time_s;
    if (time_s > period_s)
    {
        sign = - sign;
        reference = sign * amplitude;
        period_s += step_s;
    }

    //printf("%f %f\n", reference, time_s);

    return reference;
}

int
flexible_link_controller(char *data_packet, char *command_packet, double reference)
{
    int time = ((int*)data_packet)[1];
    double theta_angle = ((double*)data_packet)[1]; 
    double deflection = ((double*)data_packet)[2];

    double filtered_theta_angle = 0.0;
    double filtered_dot_theta_angle = 0.0;
    double filtered_dot_deflection = 0.0;
    double filtered_deflection = 0.0;
    double filtered_reference = 0.0;

    static int sequence_number = 0;
    sequence_number += 1;


    //  convert deflection from volts to radians
    deflection = deflection * k_gage;

    /// NOT GOOOD MORON!
    /// you need to solve this design problem
    ((double*)data_packet)[2] = deflection;

    //  filter the deflection
    filtered_deflection = filter_deflection(deflection, previous_filtered_deflection);
    
    g_plot_filtered_deflection = filtered_deflection;
    
    filtered_reference = filter_me(reference, previous_filtered_dot_reference, 40);

    g_plot_reference = filtered_reference;

    filtered_dot_theta_angle = diff_and_filter(theta_angle, previous_theta_angle, previous_filtered_dot_theta_angle, 50);
    
    filtered_dot_deflection = diff_and_filter(filtered_deflection, previous_filtered_deflection, previous_filtered_dot_deflection, 30);
    
    //printf("Deflection: %f FilteredDeflection: %f\n", filtered_deflection, global_filtered_deflection);

    ///NOTE SOMEWHERE THAT THE USER NEEDS TO PUT THE SIZE OF THE BUFFER THERE
    //command_packet[0] is reserved for now
    ((int*)command_packet)[0] = 16; //packet_size
    ((int*)command_packet)[1] = sequence_number;
    double my_command = gains[0] * (filtered_reference - theta_angle) - gains[1] * filtered_deflection - gains[2] * filtered_dot_theta_angle - gains[3] * filtered_dot_deflection;

//    ((double*)command_packet)[1] = gains[0] * (reference - theta_angle);
 //   printf("Time: %d    ThetaAngle: %f    Deflection: %f  Dot_Theta: %f  Dot_Deflection: %f\n Reference Angle: %f", time, theta_angle, deflection, dot_theta_angle, dot_deflection, reference_angle);
    //printf("%f\n", reference_angle);
    

    if (my_command >= 15.0)
    {
	((double*)command_packet)[1] = 15.0;
    }
    else if (my_command <= -15.0)
    {
	((double*)command_packet)[1] = -15.0;
    }
    else
	((double*)command_packet)[1] = my_command;

//    printf("Command: %f Reference: %f \n", my_command, filtered_reference);

    //  set previous variables
    previous_theta_angle = theta_angle;
    previous_deflection = deflection;
    previous_filtered_deflection = filtered_deflection;
    previous_filtered_dot_theta_angle = filtered_dot_theta_angle;
    previous_filtered_dot_deflection = filtered_dot_deflection;  
    previous_filtered_dot_reference = filtered_reference;

    return 0;
}


void
flexible_link_compensator(char *command_packet)
{
 //   printf("Hello world from compensator callback!\n");
	((double*)command_packet)[1] *= 0.05;
	//printf("comp. command: %f\n", ((double*)command_packet)[1]); 
}


void
flexible_link_udp_plot_packet(char *plot_packet, char *data_packet, char *command_packet)
{
    /// Beware of packet reformatting then this needs to be changed also not a good design decision moron
//    ((int *) plot_packet)[0] = ((int *) data_packet)[1];
    ((double *) plot_packet)[0] = global_time;
    ((double *) plot_packet)[1] = g_plot_reference;
    ((double *) plot_packet)[2] = ((double *) data_packet)[1] + g_plot_filtered_deflection;
    ((double *) plot_packet)[3] = ((double *) data_packet)[2];
    ((double *) plot_packet)[4] = g_plot_filtered_deflection;

}
