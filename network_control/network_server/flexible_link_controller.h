#ifndef _INVERTED_PENDULUM_CONTROLLER_H
#define _INVERTED_PENDULUM_CONTROLLER_H

#include "../common/features.h"


//              Data packets for flexible joint controller
// ----------------------------------------------------------------------------------------------------------------------------------------------- -->
// |   packet_size (32bit)  |    time / timestamp (32bit)  |   theta-angle (64bit) |   arm-deflection (64bit)    |  -->
// ----------------------------------------------------------------------------------------------------------------------------------------------- -->
// 


int flexible_link_controller(char *data_packet, char *command_packet, double reference);
double flexible_link_reference_generator(double time_s);
void flexible_link_compensator(char *command_packet);
void flexible_link_udp_plot_packet(char *plot_packet, char *data_packet, char *command_packet);

#endif // _INVERTED_PENDULUM_CONTROLLER_H