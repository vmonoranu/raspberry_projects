#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "../common/build_options.h"
#include "../common/messages.h"
#include <unistd.h>


#ifndef __arm__
#include <x86intrin.h>

#endif


int self_socket = 0;
struct sockaddr_in server_address;
pthread_t data_sender_thread;
pthread_t command_receiver_thread;

int
init_client_socket ()
{
    //
    //  Creating the client socket
    //
    if (-1 == (self_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
    {
        exit(-1);
    }

    memset((char *)&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(SERVER_PORT);

    if (0 == inet_aton(SERVER_ADDRESS, &server_address.sin_addr))
    {
        //failed to convert ASCII to network address
        exit(-1);
    }

    return 0;
}

void*
sender_thread (void* arg)
{
    int status = 0;
    char buffer[BUFFER_SIZE];
    uint32_t srv_addr_len = sizeof(server_address);


    status = sendto(self_socket, CLIENT_START_MESSAGE, strlen(CLIENT_START_MESSAGE), 0, (struct sockaddr *) &server_address, srv_addr_len);
    while (1)
    {
        for (int label = 0; label < NUMBER_OF_LABELS; ++label)
        {
            unsigned long long timestamp = 0;

            usleep(1000);
#ifndef __arm__
            timestamp = __rdtsc();
#else
            timestamp  = 0xDEADBEEF;
#endif
            sprintf(buffer, "Label: %d Timestamp: %llu", label, timestamp);
            printf("sender_thread() -> About to send: \"%s\"\n", buffer);
            status = sendto(self_socket, buffer, strlen(buffer),0, (struct sockaddr*) &server_address, srv_addr_len);

            if (-1 == status)
            {
                exit(-1);
            }
        }

        break;
    }


    return NULL;
}

void*
receiver_thread (void* arg)
{
    int status = 0;
    char buffer[BUFFER_SIZE];
    uint32_t srv_addr_len = sizeof(server_address);

    while (1)
    {
        status = recvfrom(self_socket, buffer, BUFFER_SIZE, 0, (struct sockaddr *) &server_address, &srv_addr_len);

        if (-1 == status)
        {
            printf("Failed receiving!\n");
        }

        printf("receiver_thread() -> \"%s\"\n", buffer);
    }


    return NULL;
}



int
start_sender_receiver_threads ()
{
    int status = 0;

    status = pthread_create(&data_sender_thread, NULL, &sender_thread, NULL);

    if (status)
    {
        exit(-1);
    }
    status = pthread_create(&command_receiver_thread, NULL, receiver_thread, NULL);

    if (status)
    {
        exit(-1);
    }

    return 0;
}


int
main (int argc, char* argv[])
{
    init_client_socket();
    start_sender_receiver_threads();

    printf("Waiting for client threads to finish...\n");

    pthread_join(data_sender_thread, NULL);
    pthread_join(command_receiver_thread, NULL);

    return  0;
}