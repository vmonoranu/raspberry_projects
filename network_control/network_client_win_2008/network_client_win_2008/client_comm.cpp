#include <stdio.h>
#include "client_comm.h"
#include "quanser_status.h"
#include "quanser_board.h"
#include "../../common/build_options.h"
#include <winsock.h>
#include <Windows.h>
#include <signal.h>


// PACKET FORMAT DESCRIBED IN client_comm.h

#define PI		3.141599265359


static char error_log_buffer[ERROR_LOG_BUFFER_SIZE];
static char card_identifier[] = "0";
int g_kill_all = 0;



int g_self_socket = 0;
struct sockaddr_in g_server_address;
HANDLE g_data_sender_thread;
HANDLE g_command_receiver_thread;
WSADATA g_wsaData;



void
intHandler(int dummy)
{
	g_kill_all = 1;
    t_double analog_buffer[NR_ANALOG_CHANNELS];
    t_error quarc_status = 0;

	int status = 0;
    int srv_addr_len = sizeof(g_server_address);


    char buffer[BUFFER_SIZE];

	printf("DIE!!!\n");
    for (int i = 0; i < NR_ANALOG_CHANNELS; ++i)
    {
        analog_buffer[i] = 0.0;
    }

	((int*)buffer)[0] = 0xDEADBEEF;  //including this dword
		((int*)buffer)[1] = 0xDEADBEEF;


	while(1)
	{
		quarc_status = hil_write_analog(g_quanser_card, g_analog_channels, NR_ANALOG_CHANNELS, analog_buffer);

		status = sendto(g_self_socket, buffer, 32, 0, (struct sockaddr*)&g_server_address, srv_addr_len);
	}
    

	exit(-1);
}



int
init_client_socket()
{
    unsigned long srv_address = 0;
    int result = 0;

    result = WSAStartup(MAKEWORD(2, 2), &g_wsaData);

    if (result != 0)
    {
        exit(-1);
    }

    //
    //  Creating the client socket
    //
    if (-1 == (g_self_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
    {
        exit(-1);
    }

    memset((char *)&g_server_address, 0, sizeof(g_server_address));
    g_server_address.sin_family = AF_INET;
    g_server_address.sin_port = htons(SERVER_PORT);

	if (0 == (srv_address = inet_addr(SERVER_ADDRESS)))
    {
        //failed to convert ASCII to network address
        exit(-1);
    }

    g_server_address.sin_addr.s_addr = srv_address;


	signal(SIGINT, intHandler);
    return 0;
}

//
//	This thread gets data from the quanser board and sends it to the remote controller
//
void*
sender_thread(void *arg)
{
    int status = 0;
    t_error quanser_status = 0;
    char buffer[BUFFER_SIZE];
    int srv_addr_len = sizeof(g_server_address);
    
	double theta_angle = 0.0;
	double arm_deflection = 0.0;
	int timestamp = 0;
    int size_of_data_packet = 4 + 4 + 8 + 8;
    
    t_int32 encoder_buffer[NR_CHANNEL_ENCODERS];
    t_error quarc_status = 0;

    if (!HIL_SUCCESS(quanser_status))
    {
        printf("Failed to create an encoder reader!\n");
        exit(-1);
    }


    while (1 && (g_kill_all == 0))
    {
        //	Blocking read! if we don't have any elements in the input channel
        quanser_status = hil_read_encoder(g_quanser_card, g_encoder_channels, NR_CHANNEL_ENCODERS, encoder_buffer);
		quanser_status = hil_read_analog(g_quanser_card, g_analog_input_channels, NR_ANALOG_INPUT_CHANNELS, g_analog_input);
        
        if (!HIL_SUCCESS(quanser_status))
        {
            printf("Something went wrong! see hil_read_encoder, don't be lazy and get the status code moron!\n");
        }

        //make buffer to send!
		theta_angle = encoder_buffer[0] / 4096.0 * 2.0 * PI;
		
        timestamp += 1;
        
        ((int*)buffer)[0] = size_of_data_packet;  //including this dword
		((int*)buffer)[1] = timestamp;
		((double*)buffer)[1] = theta_angle;
		((double*)buffer)[2] = g_analog_input[0];

		//printf("%f\n", ((double*)buffer)[2]);
		

		//fault detection routine
		
		if (theta_angle >= PI / 2.0 + PI / 6.0)
		{
			intHandler(0);
		}

		if (theta_angle <= -PI / 2.0 - PI / 6.0)
		{
			intHandler(0);
		}
		
		status = sendto(g_self_socket, buffer, 32, 0, (struct sockaddr*)&g_server_address, srv_addr_len);

        
        
		//printf("Data from our bedrock: %.1f\n", g_analog_input[0]);
        //printf("Data from our bedrock: %d\n", encoder_buffer[0]);
        
        ///!!! This guy won't give us a 10ms slice
        Sleep(10);
    }


    return NULL;
}

//
//  This thread gets the command packet from the remote controller
//
void*
receiver_thread(void *arg)
{
    int status = 0;
    char command_packet[BUFFER_SIZE];
    int srv_addr_len = sizeof(g_server_address);
    t_error quarc_status = 0;
    t_double analog_buffer[NR_ANALOG_CHANNELS];
    int timestamp = 0;
    int latest_timestamp = 0;
	struct timeval tv;
	fd_set readfds;
	bool changed = false;
	double latest_command = 0.0;
	

	tv.tv_sec = 0;
	tv.tv_usec = 0;

    for (int i = 0; i < NR_ANALOG_CHANNELS; ++i)
    {
        analog_buffer[i] = 0.0;
    }

    //  calm your dog and send 0 voltage to his motor
    quarc_status = hil_write_analog(g_quanser_card, g_analog_channels, NR_ANALOG_CHANNELS, analog_buffer);
    
    
    while (1 && (g_kill_all == 0))
    {
	//	Sleep(1);
		status = recvfrom(g_self_socket, command_packet, BUFFER_SIZE, 0, (struct sockaddr *) &g_server_address, &srv_addr_len);

		if (-1 == status)
		{
			continue;
		}
		latest_command = ((double*)command_packet)[1];
	/*	changed = false;
		while(1) 
		{
			FD_ZERO(&readfds);
			FD_SET(g_self_socket, &readfds);

			status = select(g_self_socket, &readfds, NULL, NULL, &tv);
			
			if (FD_ISSET(g_self_socket, &readfds))
			{

				status = recvfrom(g_self_socket, command_packet, BUFFER_SIZE, 0, (struct sockaddr *) &g_server_address, &srv_addr_len);

				timestamp = ((int*)command_packet)[1];
				//printf("%u ", timestamp);
					changed = true;
					latest_command = ((double*)command_packet)[1];
				break;
				if (latest_timestamp < timestamp)
				{
					changed = true;
					latest_command = ((double*)command_packet)[1];
					latest_timestamp = timestamp;
				}
			}
			else
			{
				break;
			}
		}

		if (changed == false)
		{
			continue;
		}
*/
        //  this plant will receive a voltage for the motor 
        
		if (latest_command >= 15.0)
		{
			latest_command = 15.0;
		}else if (latest_command <= -15.0)
		{
			latest_command = 15.0;
		}

		analog_buffer[0] = -latest_command;
		
		//printf("%f\n", analog_buffer[0]);
        quarc_status = hil_write_analog(g_quanser_card, g_analog_channels, NR_ANALOG_CHANNELS, analog_buffer);
        if (!HIL_SUCCESS(quarc_status))
        {
            msg_get_error_messageA(NULL, status, error_log_buffer, ERROR_LOG_BUFFER_SIZE);
            printf("[ERROR] %s\n", error_log_buffer);
        }
    }


    return NULL;
}



int
start_sender_receiver_threads()
{
    int status = 0;

    g_data_sender_thread = CreateThread(
        NULL,
        NULL,
        (LPTHREAD_START_ROUTINE)sender_thread,
        NULL,
        0,
        NULL);

    if (NULL == g_data_sender_thread)
    {
        exit(-1);
    }

    g_command_receiver_thread = CreateThread(
        NULL,
        NULL,
        (LPTHREAD_START_ROUTINE)receiver_thread,
        NULL,
        0,
        NULL);

    if (NULL == g_command_receiver_thread)
    {
        exit(-1);
    }

    return 0;
}

void
wait_for_sender_receiver_threads()
{
    WaitForSingleObject(
        g_data_sender_thread,
        INFINITE);
    WaitForSingleObject(
        g_command_receiver_thread,
        INFINITE);
}
