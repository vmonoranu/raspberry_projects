#include <stdio.h>
#include <Windows.h>
#include "client_comm.h"
#include "quanser_board.h"


int 
main(int argc, char *argv[])
{
	init_quanser_board();
    init_client_socket();
    start_sender_receiver_threads();

    printf("Waiting for client threads to finish...\n");

    wait_for_sender_receiver_threads();
    clean_quanser_board();

 
	return 0;
}