#!/usr/python

import sys
import os

script, file_name = sys.argv

with open(file_name, 'r') as f:
    previous_label = -1
    previous_timestamp = 0
    previous_line = ''
    for line in f:
        cmd = line.split()
        label = cmd[1]
        timestamp = cmd[3]

        #check if lines are consecutive (i.e. labelwise and timestampwise)
        if (previouse_label + 1 != label):
            print 'Difference:'
            print '    prev_line: "%s" ' % previous_line
            print '    curr_line: "%s" ' % line


        previous_label = label
        previous_timestamp = timestamp
        previous_line = line

