#ifndef SYNCH_WRAPPER_H
#define SYNCH_WRAPPER_H


#ifdef __linux__

#include <pthread.h>
#include <semaphore.h>


typedef struct _LOCK_WRAPPER
{
    unsigned char lock_type;
    union
    {
        pthread_mutex_t mutex;
        sem_t semaphore;
        pthread_spinlock_t spinlock;
    };

}LOCK_WRAPPER, *PLOCK_WRAPPER;

typedef enum _LOCK_WRAPPER_TYPE
{
    TYPE_MUTEX = 0,
    TYPE_SEMAPHORE,
    TYPE_SPINLOCK,
    TYPE_FUTEX
}LOCK_WRAPPER_TYPE;


//
//  Function headers for the LOCK_WRAPPER
//
int init_lock_wrapper(PLOCK_WRAPPER Lock, int Lock_type);
int acquire_lock_wrapper(PLOCK_WRAPPER Lock);
int release_lock_wrapper(PLOCK_WRAPPER Lock);
int clean_lock_wrapper(PLOCK_WRAPPER Lock);

#endif

#endif //SYNCH_WRAPPER_H
