#ifndef _FEATURES_PROJ_H
#define _FEATURES_PROJ_H

#define sizeof_array(ARRAY) (sizeof(ARRAY) / sizeof(ARRAY[0]))


typedef int (*FN_CONTROLLER_PROCESS_DATA)(char *data_packet, char *command_packet, double reference);
typedef double (*FN_CONTROLLER_GENERATE_REFERENCE)(double time_s);
typedef void (*FN_CONTROLLER_COMPENSATOR)(char *command_packet);
typedef void (*FN_CONTROLLER_UDP_PLOT)(char *plot_packet, char *data_packet, char *command_packet);

#endif //_FEATURES_H
